require gammaspc
require essioc
require iocmetadata

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("P", "MBL-050RFC:")
epicsEnvSet("R1", "RFS-VacPS-310:")
epicsEnvSet("IP1", "mbl5-rf3-ip1.tn.esss.lu.se:23")
epicsEnvSet("PORT1", "GAMMA1")

epicsEnvSet("R2", "RFS-VacPS-320:")
epicsEnvSet("IP2", "mbl5-rf3-ip2.tn.esss.lu.se:23")
epicsEnvSet("PORT2", "GAMMA2")


iocshLoad("$(gammaspc_DIR)/gammaSpce.iocsh", "R=$(R1), IP=$(IP1), PORT=$(PORT1)")
iocshLoad("$(gammaspc_DIR)/gammaSpce.iocsh", "R=$(R2), IP=$(IP2), PORT=$(PORT2)")

pvlistFromInfo("ARCHIVE_THIS","$(IOCNAME):ArchiverList")

